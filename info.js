const express = require("express");
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


  app.listen(4000, () => {
  console.log("El servidor está inicializado en el puerto 4000");
  });

  app.get('/info', function (req, res) {

    let informacion ={
      nombre:"Kevin Alfredo Lopez Rodriguez",
      carne:201901016
    };
    res.send(informacion)
  });