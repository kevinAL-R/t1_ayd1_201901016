const express = require("express");
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


  app.listen(3000, () => {
  console.log("El servidor está inicializado en el puerto 3000");
  });
  app.get('/', function (req, res) {
    res.send('hola mundo');
  });
  app.post('/suma', function (req, res) {
    let num1 = req.body.numero1
    let num2= req.body.numero2
    let resultado ={
      resultado:num1+num2
    };
    res.send(resultado)
  });
  app.post('/resta', function (req, res) {
    let num1 = req.body.numero1
    let num2= req.body.numero2
    let resultado ={
      resultado:num1-num2
    };
    res.send(resultado)
  });
  app.post('/multi', function (req, res) {
    let num1 = req.body.numero1
    let num2= req.body.numero2
    let resultado ={
      resultado:num1*num2
    };
    res.send(resultado)
  });